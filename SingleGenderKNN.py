# Code adapted from
#http://www.programmersought.com/article/7592512303/
#https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
#https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html
#https://scikit-learn.org/stable/auto_examples/neighbors/plot_classification.html#sphx-glr-auto-examples-neighbors-plot-classification-py
#https://medium.com/usf-msds/choosing-the-right-metric-for-evaluating-machine-learning-models-part-2-86d5649a5428

from sklearn.model_selection import train_test_split
import csv
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn import neighbors
from sklearn.metrics import f1_score


differentClasses = 3

uniformf1scores = []
distancef1scores = []


# Value of k
n_neighbors = 5

# number of times to run the program
noOfIterations = 100

#Running the test for noOfIterations times
for i in range(noOfIterations):
    # Opening the file with the normalized data of the gender we want to generate the knn from
    with open('Data/NormalizedData/FemaleNormalizedData.csv') as csv_file:
        # Reading from the CSV file the normalized data
        csv_reader = csv.reader(csv_file, delimiter=',')
        counter = 0
        lineCounter = 0
        xdata = np.zeros((75, 3))
        ydata = np.zeros(75)



        # Going through each row in the CSV file
        for row in csv_reader:
            # If the row is not the first row (i.e. title row)
            if lineCounter != 0:

                # Gathering all the required data and storing it in np arrays
                # The following is collected, vowel phoneme class number, formant 1, formant 2, formant 3

                ydata[counter] = row[5]
                xdata[counter][0] = float(row[7])
                xdata[counter][1] = float(row[8])
                xdata[counter][2] = float(row[9])
                counter +=1

            lineCounter+=1.

        # Generating the training and testing data
        # Each time the program is run, the resulting sets of data are different hence giving a random element
        X_train, X_test, y_train, y_test = train_test_split(xdata, ydata, test_size=0.25)



    # Array to store the predicted answers
    predans = np.zeros(19)


    # Setting the X and y values to the training set deduced above
    X = X_train
    y = y_train


    for weights in ['uniform', 'distance']:
        # Creating an instance of Neighbours Classifier and fitting the data.
        clf = neighbors.KNeighborsClassifier(n_neighbors, weights=weights,metric="manhattan")
        clf.fit(X, y)

        # Going through each value in the testing set and store its prediction
        for i,testData in enumerate(X_test):
            predans[i] = (clf.predict([testData]))

        # Generating and displaying a confusion matrix, showing the difference between the predicated and actual values
        print(weights + " weight function")
        print()
        confMatrix = confusion_matrix(y_test,predans,[1,2,3])
        print("Confusion Matrix")
        print(confMatrix)
        print()

        # Generating an F1 score for the confusion matrix
        # Macro was used since Micro is used when a class imbalance exists
        f1score = f1_score(y_test, predans, [1, 2, 3], average="macro")
        print(u"F\u2081 Score = ", f1score)
        print("------------------")
        if weights == "uniform":
            uniformf1scores.append(f1score)
        elif weights == "distance":
            distancef1scores.append(f1score)

print(u"Unifrom Average F\u2081 Score = ", sum(uniformf1scores)/len(uniformf1scores))
print(u"Distance Average F\u2081 Score = ", sum(distancef1scores)/len(distancef1scores))
