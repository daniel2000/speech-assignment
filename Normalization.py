# Code adapted from
# https://machinelearningmastery.com/normalize-standardize-time-series-data-python/

import csv
# These minimum and maximum values are collected from the previous py file, Average.py
max = 3599.253661
min = 204.096814
counter = 0
values = []
values.append([])
values.append([])
values.append([])
values.append([])

rowToWrite = []

# Opening a file to write the normalized data to
writeFile = open("Data/NormalizedData/MaleNormalizedData.csv", "w")

# Opening the csv file
with open('Data/Raw Data/MaleData.csv') as csv_file:
    # Initializing csv readers and writers
    csv_reader = csv.reader(csv_file, delimiter=',')
    csv_writer = csv.writer(writeFile,lineterminator = '\n')
    line_count = 0

    # Writing the title row to the new csv file
    csv_writer.writerow(["Directory","Speaker Label","Gender","Word","Vowel Phoneme","Class Number","Time Marker","Formant 1","Formant 2","Formant 3"])
    # Going through each row
    for row in csv_reader:
        # Ignoring the title row
        if line_count != 0:

            for j in range (len(row)):
                # Adding all the data to the new row to be written from column 0 to column 6
                if j < 7:
                    rowToWrite.append(row[j])

                else:
                    # Normalizing the 3 formant values and adding them to the new row to be written
                    y = (float(row[j]) - min) / (max - min)
                    rowToWrite.append(y)

            # Writing the new row to the file
            csv_writer.writerow(rowToWrite)
            rowToWrite.clear()
            counter = 0

        line_count +=1

