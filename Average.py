# Code adapted from
# https://machinelearningmastery.com/normalize-standardize-time-series-data-python/

import csv
max = 0
min = 1000
counter = 0
values = []
values.append([])
values.append([])
values.append([])
values.append([])

rowToWrite = []

# Opening a file to write to
writeFile = open("Data/Raw Data/AverageData.csv","w")

# Reading from the csv file
with open('Data/Raw Data/DataExtraction.csv') as csv_file:
    # Creating instances of a csv reader and writer
    csv_reader = csv.reader(csv_file, delimiter=',')
    csv_writer = csv.writer(writeFile,lineterminator = '\n')
    line_count = 0

    # Writing the title row to the new file
    csv_writer.writerow(["Directory","Speaker Label","Gender","Word","Vowel Phoneme","Class Number","Time Marker","Formant 1","Formant 2","Formant 3"])
    # Going through each row in the file
    for row in csv_reader:
        # Ignoring the title row
        if line_count != 0:
            counter += 1
            # Adding the time, F1, F2 and F3 values to a list
            for i, value in enumerate(values):
                value.append(float(row[i+6]))

        if counter == 10:
            # Once all the time, F1, F2, F3 values have been collected
            # Writing all the columns before the values.
            for j in range (6):
                rowToWrite.append(row[j])

            # Going through each list holding the times, F1, F2 and F3 values
            for i, value in enumerate(values):
                # Skipping the first list, since this is the time value which cannot be normalized and therefore no point in finding its min and max
                if i != 0:
                    if (sum(value)/len(value)) > max:
                        max = sum(value)/len(value)
                    if sum(value)/len(value) < min:
                        min = sum(value)/len(value)
                # Taking the average of the current 10 values
                rowToWrite.append((sum(value)/len(value)))
                value.clear()

            # Writing the new row to the file
            csv_writer.writerow(rowToWrite)
            rowToWrite.clear()
            counter = 0

        line_count +=1

# Printing the maximum and minimum from the 3 formant values
print("Max",max)
print("min",min)