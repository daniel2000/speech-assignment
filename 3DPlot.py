# Code adapted from
# http://www.programmersought.com/article/7592512303/

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import csv

F1 = []
F2 = []
F3 = []
y = []

noOfDataPoints = 0

# Opening the csv file with the normalized data
with open('Data/NormalizedData/NormalizedData.csv') as csv_file:
    # Reading from the CSV file
    csv_reader = csv.reader(csv_file, delimiter=',')
    lineCounter = 0

    # Going through each row in the CSV file
    for row in csv_reader:
        # If the row is not the first row (i.e. title row)
        if lineCounter != 0:
            # Adding each value to a separate list, for use by the scatter function
            y.append(float(row[5]))
            F1.append(float(row[7]))
            F2.append(float(row[8]))
            F3.append(float(row[9]))
            noOfDataPoints += 1

        lineCounter += 1

# Assigning the y values to the correct colour based on their value
label_color_dict = {1: 'red', 2: 'green', 3: 'blue'}
colors = list(map(lambda label: label_color_dict[label], y))

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
# Plotting the points
ax.scatter(F1, F2, F3, c=colors, marker='o', s=100)
ax.set_xlabel('Formant 1')
ax.set_ylabel('Formant 2')
ax.set_zlabel('Formant 3')

# Creating the legend
pop_a = mpatches.Patch(color='red', label='AA')
pop_b = mpatches.Patch(color='green', label='IY')
pop_c = mpatches.Patch(color='blue', label='OY')
plt.legend(handles=[pop_a, pop_b, pop_c])

# Showing the 3d plot
plt.show()

# Displaying the total number of data points that were plotted
print("Total no of Data points: " , noOfDataPoints)
