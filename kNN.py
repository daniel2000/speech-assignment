# Code adapted from
# http://www.programmersought.com/article/7592512303/
# https://scikit-learn.org/stable/modules/generated/sklearn.neighbors.KNeighborsClassifier.html
# https://scikit-learn.org/stable/modules/generated/sklearn.metrics.f1_score.html
# https://scikit-learn.org/stable/auto_examples/neighbors/plot_classification.html#sphx-glr-auto-examples-neighbors-plot-classification-py
# https://medium.com/usf-msds/choosing-the-right-metric-for-evaluating-machine-learning-models-part-2-86d5649a5428

from sklearn.model_selection import train_test_split
import csv
from sklearn.metrics import confusion_matrix
import numpy as np
from sklearn import neighbors
from sklearn.metrics import f1_score


def getData(differentClasses):
    # Initialzing numpy arrays to store the data
    # It is a 3 by 25 by 3 array since we have 3 classes each having 25 data points and each of the data point having 3 different formant values
    xMaleData = np.zeros((3, 25, 3))
    xFemaleData = np.zeros((3, 25, 3))
    # 3 by 25 since we have 3 classes each having 25 data points and each data point has only 1 y value (the class number)
    yMaleData = np.zeros((3, 25))
    yFemaleData = np.zeros((3, 25))

    # Going through each vowel phoneme class number
    for z in range(1, differentClasses + 1):
        # Opening the file
        with open('Data/NormalizedData/NormalizedData.csv') as csv_file:
            # Reading from the CSV file the normalized data
            csv_reader = csv.reader(csv_file, delimiter=',')
            counterM = 0
            counterF = 0
            lineCounter = 0

            # Going through each row in the CSV file
            for row in csv_reader:
                # If the row is not the first row (i.e. title row) and the vowel phoneme class number is equal to the current one
                if lineCounter != 0 and int(row[5]) == z:

                    # Since the training and testing set need to be divided 75/25 for each gender (for each class),
                    # the two gender's data is collected separately
                    # The following is collected, vowel phoneme class number, formant 1, formant 2, formant 3

                    # Female data
                    if row[2] is "F":
                        # Storing the Class number in the y array
                        yFemaleData[z - 1][counterF] = row[5]
                        # Storing the formant values in the x array
                        xFemaleData[z - 1][counterF][0] = float(row[7])
                        xFemaleData[z - 1][counterF][1] = float(row[8])
                        xFemaleData[z - 1][counterF][2] = float(row[9])
                        counterF += 1

                    # Male data
                    elif row[2] is "M":
                        # Storing the Class number in the y array
                        yMaleData[z - 1][counterM] = row[5]
                        # Storing the formant values in the x array
                        xMaleData[z - 1][counterM][0] = float(row[7])
                        xMaleData[z - 1][counterM][1] = float(row[8])
                        xMaleData[z - 1][counterM][2] = float(row[9])
                        counterM += 1

                lineCounter += 1
    # The x male and female data and y male and female data read from the file are returned
    return xMaleData, xFemaleData, yMaleData, yFemaleData


def computekNN(noOfIterations, n_neighbors, differentClasses, xMaleData, xFemaleData, yMaleData, yFemaleData):
    uniformf1scores = []
    distancef1scores = []

    # Running the test noOfIterations times
    for i in range(noOfIterations):
        # Going through each set of data for the different classes
        for z in range(differentClasses):

            # Generating the male and female training and testing data
            # Each time the program is run, the resulting sets of data are different hence giving a random element
            # z represents the different class numbers, Each np array passed to this function has 3 arrays within it to represent the 3 classes

            # This allows the data to be split per gender per class where in 1 iteration of this loop,
            # 75% of the male data of a particular class is chosen for the training set and the remaining is chosen as the testing set.
            # This is also done for the female data of the particular class of the current iteration.
            X_trainF, X_testF, y_trainF, y_testF = train_test_split(xFemaleData[z], yFemaleData[z], test_size=0.25)
            X_trainM, X_testM, y_trainM, y_testM = train_test_split(xMaleData[z], yMaleData[z], test_size=0.25)

            if z == 0:
                # When adding the data for the first time,
                # The set of data for the two genders are combined together
                X_train = np.vstack((X_trainM, X_trainF))
                X_test = np.vstack((X_testM, X_testF))

                # Since the y values are only 1D array, hstack was used instead of vstack
                y_train = np.hstack((y_trainM, y_trainF))
                y_test = np.hstack((y_testM, y_testF))

            else:
                # When adding further data, data of another class
                # The set of data for the two genders are combined together and with the previous data
                X_train = np.vstack((X_train, X_trainM, X_trainF))
                X_test = np.vstack((X_test, X_testM, X_testF))

                y_train = np.hstack((y_train, y_trainM, y_trainF))
                y_test = np.hstack((y_test, y_testM, y_testF))

        # Array to store the predicted answers
        predans = np.zeros(42)

        # Setting the X and y values to the training set deduced above
        X = X_train
        y = y_train

        for weights in ['uniform', 'distance']:
            # Creating an instance of Neighbours Classifier and fitting the data.
            clf = neighbors.KNeighborsClassifier(n_neighbors, weights=weights, metric="manhattan")
            clf.fit(X, y)

            # Going through each value in the testing set and storing its prediction
            for i, testData in enumerate(X_test):
                predans[i] = (clf.predict([testData]))

            # Generating and displaying a confusion matrix, showing the difference between the predicated and actual values
            print(weights + " weight function")
            print()
            confMatrix = confusion_matrix(y_test, predans, [1, 2, 3])
            print("Confusion Matrix")
            print(confMatrix)
            print()

            # Generating an F1 score for the confusion matrix
            # Macro was used since Micro is used when a class imbalance exists
            f1score = f1_score(y_test, predans, [1, 2, 3], average="macro")
            print(u"F\u2081 Score = ", f1score)
            print("------------------")
            if weights == "uniform":
                uniformf1scores.append(f1score)
            elif weights == "distance":
                distancef1scores.append(f1score)

    # Showing the average F1 scores for all the classifiers generated
    print(u"Unifrom Average F\u2081 Score = ", sum(uniformf1scores) / len(uniformf1scores))
    print(u"Distance Average F\u2081 Score = ", sum(distancef1scores) / len(distancef1scores))


# No of different Labels/Classes
differentClasses = 3

# Value of k
k = 5

# number of times to run the program
noOfIterations = 10

#Obtaining the data
xMaleData, xFemaleData, yMaleData, yFemaleData = getData(differentClasses)

#Computing the kNN
computekNN(noOfIterations, k, differentClasses, xMaleData, xFemaleData, yMaleData, yFemaleData)
